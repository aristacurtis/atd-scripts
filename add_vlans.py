#!/usr/bin/env python3

from jsonrpclib import Server
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

nodes = ["192.168.0.10","192.168.0.11","192.168.0.12","192.168.0.13","192.168.0.14","192.168.0.15","192.168.0.16","192.168.0.17"]

vlans = [
    {"id": 100, "name": "DATA", "active": "true"},
    {"id": 101, "name": "PHONE", "active": "false"},
    {"id": 102, "name": "SERVERS", "active": "true"},
]

username = "arista"
password = "CHANGEME"

for node in nodes:

    switch = Server ("https://" + username + ":" + password + "@" + node + "/command-api")

    for vlan in vlans:
        vlanId = vlan['id']
        vlanName = vlan['name']
        if vlan['active'] == "true":
            vlanState = "active"
        else:
            vlanState = "suspended"
    
        response = switch.runCmds( 1, ["configure",
                                        "vlan %s" %vlanId, 
                                        "  name %s" %vlanName,
                                        "  state %s" %vlanState
        ] )
    response2 = switch.runCmds( 1, ["show vlan 100",
                                    "show vlan 101",
                                    "show vlan 102"])
    
    print(response2)
