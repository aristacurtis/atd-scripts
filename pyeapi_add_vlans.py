#!/usr/bin/env python3

import pyeapi
import pprint

switches = ["192.168.0.10","192.168.0.11","192.168.0.12","192.168.0.13","192.168.0.14","192.168.0.15","192.168.0.16","192.168.0.17"]

vlans = [
    {"id": 200, "name": "pyeAPI_DATA"},
    {"id": 201, "name": "pyeAPI_PHONE"},
    {"id": 202, "name": "pyeAPI_SERVERS"},
]

user = "arista"
pass = "CHANGEME"

for switch in switches:

    node = pyeapi.connect(host=switch,username=user,password=pass,return_node=True)    
    vlansAPI = node.api('vlans')

    for vlan in vlans:
        vlansAPI.create(vlan['id'])
        vlansAPI.set_name(vlan['name'])

    allVlans = vlansAPI.getall()
    pprint.pprint(allVlans)
    print('========================')


