#!/usr/bin/env python3

from jsonrpclib import Server
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

switch = Server ("https://arista:CHANGEME@192.168.0.12/command-api")

response = switch.runCmds( 1, ["show version"] )

print(response)
